//
//  Constants.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

final class Constants {
    
    static let apiBase = "https://api.edadev.ru/intern/"
    
    struct Server {
        static var baseParams: RequestParams {
            let baseParams = [String:String]()

            return baseParams
        }
    }
}
