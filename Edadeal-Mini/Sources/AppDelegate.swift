//
//  AppDelegate.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        if let window = window {
            window.backgroundColor = UIColor.white
            
            let tabBarController = UITabBarController()
            
            let searchVC = SearchViewController()
            searchVC.title = "Поиск"
            let purshaseVC = PurchasesViewController()
            purshaseVC.title = "Покупки"
            
            tabBarController.viewControllers = [UINavigationController(rootViewController: searchVC), UINavigationController(rootViewController: purshaseVC)]
            tabBarController.tabBar.items![0].image = #imageLiteral(resourceName: "search_icon")
            tabBarController.tabBar.items![1].image = #imageLiteral(resourceName: "shopping-cart_icon")
            
            window.rootViewController = tabBarController
            window.makeKeyAndVisible()
        }
        
        RealmManager.shared.setup()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

