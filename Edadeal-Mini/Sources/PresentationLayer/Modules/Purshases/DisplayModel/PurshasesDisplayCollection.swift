//
//  PurshasesDisplayCollection.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 11.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

class PurshasesDisplayCollection: DisplayCollection {
    
    var numberOfSections: Int = 1
    
    @objc weak var delegate: DisplayCollectionDelegate?
    
    public var products = [ProductEntity]()
    
    init() {
        fetchProducts()
        
        NotificationCenter.default.addObserver(self, selector: #selector(fetchProducts), name: NSNotification.Name(rawValue: "FetchPurshases"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FetchPurshases"), object: nil)
    }
    
    func updateProductsSection(with products: [ProductEntity]) {
        self.products = products
        delegate?.updateUI()
    }
    
    func numberOfRows(in section: Int) -> Int {
        return products.count
    }
    
    func model(for indexPath: IndexPath) -> CellViewModel {
        return ProductTableViewCellModel(entity: products[indexPath.row])
    }
    
    @objc func fetchProducts() {
        let products = mainRealm.objects(ProductEntity.self).filter("isInCart == true")
        updateProductsSection(with: Array.init(products))
    }
}
