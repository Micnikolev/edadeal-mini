//
//  SearchDisplayCollection.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

@objc class SearchDisplayCollection: NSObject, DisplayCollection {

    var numberOfSections: Int = 1
    
    private var searchMask = ""
    
    @objc weak var delegate: DisplayCollectionDelegate?
    
    public var products = [ProductEntity]()
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(fetchProducts), name: NSNotification.Name(rawValue: "FetchProducts"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FetchProducts"), object: nil)
    }
    
    func updateProductsSection(with products: [ProductEntity]) {
        self.products = products
        delegate?.updateUI()
    }
    
    func numberOfRows(in section: Int) -> Int {
        return products.count
    }
    
    func model(for indexPath: IndexPath) -> CellViewModel {
        return ProductTableViewCellModel(entity: products[indexPath.row])
    }
    
    @objc func deleteProducts() {
        searchMask = ""
        updateProductsSection(with: [])
    }
    
    @objc func fetchProductsWidthMask(_ mask: String) {
        searchMask = mask
        fetchProducts()
    }
    
    @objc func fetchProducts() {
        ProductsFetching.fetchElements(request: ProductPlainObject.Requests.product(), completion: {
            [unowned self] in
            let products = mainRealm.objects(ProductEntity.self).filter("isInCart == false")
            
            self.updateProductsSection(with: self.filterProductsByMask(products: Array.init(products)))
        })
    }
    
    func filterProductsByMask(products: [ProductEntity]) -> [ProductEntity] {
        let arrayOfProductsWithMask = products.filter {
            $0.productDescription.localizedCaseInsensitiveContains(self.searchMask)
        }.sorted {
            $0.productDescription.index(of: self.searchMask) < $1.productDescription.index(of: self.searchMask)
        }
        
        return arrayOfProductsWithMask
    }
}

