//
//  SearchViewController.m
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

#import "SearchViewController.h"
#import "Edadeal_Mini-Swift.h"

@interface SearchViewController()<DisplayCollectionDelegate, UISearchResultsUpdating>
@end

@implementation SearchViewController

UIActivityIndicatorView *activityIndicator;
UISearchController *searchController;

SearchDisplayCollection *displayCollection;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    displayCollection = [[SearchDisplayCollection alloc] init];
    displayCollection.delegate = self;
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:activityIndicator];
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    [searchController.searchBar setPlaceholder:@"Введите название продукта"];
    searchController.searchResultsUpdater = self;
    [searchController setHidesNavigationBarDuringPresentation:false];
    [searchController setDimsBackgroundDuringPresentation:false];
    [[searchController searchBar] sizeToFit];
    self.tableView.tableHeaderView = searchController.searchBar;
    [self setDefinesPresentationContext:true];
    
    [[self tableView] registerClass: ProductTableViewCell.class forCellReuseIdentifier: @"ProductTableViewCell"];
    [[self tableView] setRowHeight:UITableViewAutomaticDimension];
    [[self tableView] setAllowsSelection:false];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[[self tabBarController] navigationItem] setTitle:@"Поиск"];
    
    if (searchController.searchBar.text.length >= 2) {
        [activityIndicator startAnimating];
        [displayCollection fetchProductsWidthMask:searchController.searchBar.text];
    }
}

-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    activityIndicator.center = self.view.center;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return displayCollection.numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [displayCollection numberOfRowsIn:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id model = [displayCollection modelFor:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellFor:indexPath with:model];
    
    return cell;
}

-(void)updateUI {
    [activityIndicator stopAnimating];
    [[self tableView] reloadData];
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if(searchController.searchBar.text.length >= 2) {
        [activityIndicator startAnimating];
        [displayCollection fetchProductsWidthMask:searchController.searchBar.text];
    } else {
        [displayCollection deleteProducts];
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [searchController.searchBar resignFirstResponder];
}

@end
