//
//  ProductTableViewCell.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell, ConfigurableCell {
    
    var descriptionLabel: UILabel!
    var retailerLabel: UILabel!
    var priceLabel: UILabel!
    var discountLabel: UILabel!
    var productImageView: UIImageView!
    var mainOptionButton: UIButton!
    var addToCartAction: (() -> Void)!
    
    var entity: ProductEntity!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initElements()
        configureElements()
        initConstraints()
    }
    
    func initElements() {
        descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 2
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(descriptionLabel)
        
        retailerLabel = UILabel()
        retailerLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(retailerLabel)
        
        priceLabel = UILabel()
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(priceLabel)
        
        discountLabel = UILabel()
        discountLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(discountLabel)
        
        productImageView = UIImageView(image: #imageLiteral(resourceName: "picture_icon"))
        productImageView.clipsToBounds = true
        productImageView.contentMode = .scaleAspectFill
        productImageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(productImageView)
        
        mainOptionButton = UIButton()
        mainOptionButton.addTarget(self, action: #selector(mainOptionButtonPressed), for: UIControlEvents.touchUpInside)
        mainOptionButton.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(mainOptionButton)
    }
    
    func configureElements() {
        descriptionLabel.text = "Description"
        retailerLabel.text = "Retailer"
        priceLabel.text = "Price"
        discountLabel.text = "Discount"
    }
    
    func initConstraints() {
        descriptionLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: self.productImageView.leadingAnchor, constant: -20).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: self.retailerLabel.topAnchor, constant: -20).isActive = true
        
        retailerLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20).isActive = true
        retailerLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 20).isActive = true
        retailerLabel.topAnchor.constraint(equalTo: self.descriptionLabel.bottomAnchor, constant: 20).isActive = true
        retailerLabel.bottomAnchor.constraint(equalTo: self.priceLabel.topAnchor, constant: -20).isActive = true
        
        priceLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20).isActive = true
        priceLabel.trailingAnchor.constraint(equalTo: self.discountLabel.leadingAnchor, constant: -20).isActive = true
        priceLabel.topAnchor.constraint(equalTo: self.retailerLabel.bottomAnchor, constant: 20).isActive = true
        
        discountLabel.leadingAnchor.constraint(equalTo: self.priceLabel.trailingAnchor, constant: 20).isActive = true
        discountLabel.trailingAnchor.constraint(equalTo: self.productImageView.leadingAnchor, constant: -30).isActive = true
        discountLabel.topAnchor.constraint(equalTo: self.priceLabel.topAnchor, constant: 0).isActive = true
        discountLabel.bottomAnchor.constraint(equalTo: self.priceLabel.bottomAnchor, constant: 0).isActive = true
        discountLabel.textAlignment = .left
        discountLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: UILayoutConstraintAxis.horizontal)
        
        productImageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -20).isActive = true
        productImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        productImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        productImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20).isActive = true
        
        mainOptionButton.leadingAnchor.constraint(equalTo: self.productImageView.leadingAnchor, constant: 0).isActive = true
        mainOptionButton.trailingAnchor.constraint(equalTo: self.productImageView.trailingAnchor, constant: 0).isActive = true
        mainOptionButton.topAnchor.constraint(equalTo: self.productImageView.bottomAnchor, constant: 20).isActive = true
        mainOptionButton.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20).isActive = true
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func mainOptionButtonPressed(sender: UIButton) {
        realmWrite {
            [unowned self] in
            if self.entity.isInCart {
                self.entity.isInCart = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FetchPurshases"), object: nil)
            } else {
                self.entity.isInCart = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FetchProducts"), object: nil)
            }
        }
    }
}

extension ProductTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
