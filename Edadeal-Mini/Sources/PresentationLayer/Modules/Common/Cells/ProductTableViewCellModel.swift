//
//  ProductTableViewCell.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

class ProductTableViewCellModel: CellViewModel {
    typealias CellClass = ProductTableViewCell
    
    var entity: ProductEntity
    
    init(entity: ProductEntity) {
        self.entity = entity
    }
    
    func setup(on cell: ConfigurableCell) {
        guard let cell = cell as? ProductTableViewCell else { return }
        cell.entity = entity
        cell.descriptionLabel.text = entity.productDescription
        cell.retailerLabel.text = (entity.retailer != nil) ? entity.retailer! :
        "Неизвестный продавец"
        
        cell.priceLabel.text = (entity.price != nil && entity.price != 0) ? String(describing: Double(truncating: entity.price!).roundTo(places: 1)) + " р." : ""
        
        cell.discountLabel.text = (entity.discount != nil && entity.discount != 0) ?
            "Скидка: " + String(describing: Double(truncating: entity.discount!).roundTo(places: 1)) + "%" : ""
        
        if let photoURL = entity.image, let url = URL(string: photoURL) {
            cell.productImageView.loadImage(from: url)
        }
        
        let mainOptionButtonImage = entity.isInCart ? #imageLiteral(resourceName: "garbage_icon") : #imageLiteral(resourceName: "cartImage")
        cell.mainOptionButton.setImage(mainOptionButtonImage, for: .normal)
    }
    
    func cellClass() -> UIView.Type {
        return ProductTableViewCell.self
    }
}
