//
//  CellViewModel.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

@objc protocol CellViewModel {
    func cellClass() -> UIView.Type
    func setup(on cell: ConfigurableCell)
}

@objc protocol ConfigurableCell {}
