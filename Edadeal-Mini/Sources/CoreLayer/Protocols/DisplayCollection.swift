//
//  DisplayCollection.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//
import Foundation

@objc protocol DisplayCollection {
    var numberOfSections: Int { get }
    func numberOfRows(in section: Int) -> Int
    func model(for indexPath: IndexPath) -> CellViewModel
}
