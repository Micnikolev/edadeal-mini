//
//  String+Index.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 13.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

extension String {
    func index(of string: String, options: CompareOptions = .caseInsensitive) -> Index {
        return range(of: string, options: options)!.lowerBound
    }
}
