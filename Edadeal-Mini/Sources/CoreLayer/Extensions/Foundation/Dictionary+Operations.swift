//
//  Dictionary+Operations.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//
import Foundation

extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {
    
    static func += (lhs: inout [Key: Value], rhs: [Key: Value]) {
        rhs.forEach {
            lhs[$0] = $1
        }
    }
    
}

