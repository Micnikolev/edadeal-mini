//
//  Double+Rounding.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 11.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
