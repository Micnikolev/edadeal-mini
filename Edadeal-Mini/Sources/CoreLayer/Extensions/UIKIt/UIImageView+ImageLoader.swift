//
//  UIImageView+ImageLoader.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 11.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadImage(from url: URL,
                   loader: AnyImageLoader = KingfisherImageLoader.standard,
                   placeholder: UIImage? = nil,
                   progressBlock: ImageLoader.ProgressBlock? = nil,
                   completionHandler: ImageLoader.CompletionBlock? = nil
        ) {
        
        loader.load(into: self, from: url,
                    placeholder: placeholder, progressBlock: progressBlock, completionHandler: completionHandler)
    }
    
    func cancelLoadImage(_ loader: AnyImageLoader = KingfisherImageLoader.standard) {
        loader.cancel(self)
    }
}
