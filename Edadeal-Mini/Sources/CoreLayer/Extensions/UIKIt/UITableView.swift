//
//  UITableView.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//
import UIKit.UITableView

@objc extension UITableView {
    
    func dequeueReusableCell(for indexPath: IndexPath, with model: CellViewModel) -> UITableViewCell {
        
        let cellIdentifier = String(describing: model.cellClass())
        let cell = dequeueReusableCell(withIdentifier: cellIdentifier,
                                       for: indexPath)
        
        guard let configCell = cell as? ConfigurableCell else { return cell }
        model.setup(on: configCell)
        
        return cell
    }
}
