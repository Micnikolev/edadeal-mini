//
//  RatesPlainObject.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

struct ProductPlainObject: PlainObjectType {
    let description: String
    let price: Double?
    let discount: Double?
    let image: String?
    let retailer: String?
    
    struct Requests {
        static func product() -> Request<[ProductPlainObject]> {
            return Request(query: "")
        }
    }
}
