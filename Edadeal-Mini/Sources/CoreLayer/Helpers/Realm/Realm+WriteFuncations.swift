//
//  Realm+WriteFuncations.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 09.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    public func realmWrite(_ block: (() -> Void)) {
        if isInWriteTransaction {
            block()
        } else {
            do {
                try write(block)
            } catch {
                assertionFailure("Realm write error: \(error)")
            }
        }
    }
}

func realmWrite(realm: Realm = mainRealm, _ block: (() -> Void)) {
    realm.realmWrite(block)
}
