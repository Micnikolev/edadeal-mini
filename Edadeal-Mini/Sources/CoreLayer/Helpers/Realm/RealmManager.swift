//
//  RealmManager.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 09.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation
import RealmSwift

var mainRealm: Realm!

class RealmManager {
    
    static var shared: RealmManager = RealmManager()
    
    func setup() {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(schemaVersion: 4, migrationBlock: nil)
        
        do {
            mainRealm = try Realm()
        } catch let error as NSError {
            assertionFailure("Realm loading error: \(error)")
        }
    }
}
