//
//  DisplayCollectionDelegate.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

@objc protocol DisplayCollectionDelegate: class {
    func updateUI()
}

