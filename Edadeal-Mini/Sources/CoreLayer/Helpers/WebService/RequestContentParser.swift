//
//  RequestContentParser.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

struct RequestContentParser<T> {
    var parseLogic: (Any) -> (T?, ServerError?)
}
