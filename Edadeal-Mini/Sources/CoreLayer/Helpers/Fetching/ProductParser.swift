//
//  ProductParser.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 09.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

class ProductParser {
    static func parseProducts(with data: Data) -> [ProductPlainObject]? {
        do {
            return try JSONDecoder().decode([ProductPlainObject].self, from: data)
        } catch  {
            print(error)
            return nil
        }
    }
}
