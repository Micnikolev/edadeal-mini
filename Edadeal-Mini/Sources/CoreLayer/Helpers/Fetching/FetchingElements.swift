//
//  FetchingElements.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 09.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

protocol FetchingElements {
    associatedtype Value = PlainObjectType
    
    static func fetchElements(request: Request<[Value]>, completion: (() -> Void)?)
}
