//
//  ProductsFetching.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 09.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

class ProductsFetching: NSObject, FetchingElements {
    static func fetchElements(request: Request<[ProductPlainObject]>, completion: (() -> Void)? = nil) {
        Server.standard.request(request, completion: { data, error in
            
            defer {
                DispatchQueue.main.async {
                    completion?()
                }
            }
            
            guard let data = data, error == nil else { return }
            
            if let parsedProducts = ProductParser.parseProducts(with: data) {
                realmWrite {
                    parsedProducts.forEach {
                        let entity = ProductEntity()
                        entity.productDescription = $0.description
                        if let discount = $0.discount {
                            entity.discount = NSNumber(value: discount)
                        }
                        if let price = $0.price {
                            entity.price = NSNumber(value: price)
                        }
                        entity.image = $0.image
                        entity.retailer = $0.retailer
                        
                        if let object = mainRealm.object(ofType: ProductEntity.self, forPrimaryKey: entity.productDescription) {
                            entity.isInCart = object.isInCart
                        }
                        
                        mainRealm.add(entity, update: true)
                    }
                }
            }
        })
    }
}
