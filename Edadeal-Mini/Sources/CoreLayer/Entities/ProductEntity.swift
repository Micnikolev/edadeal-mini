//
//  ProductEntity.swift
//  Edadeal-Mini
//
//  Created by Michael Nikolaev on 08.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation
import RealmSwift

final class ProductEntity: Object {
    @objc dynamic var productDescription: String! = ""
    @objc dynamic var price: NSNumber? = 0.0
    @objc dynamic var discount: NSNumber? = 0.0
    @objc dynamic var image: String? = nil
    @objc dynamic var retailer: String? = nil
    @objc dynamic var isInCart: Bool = false
    
    override static func primaryKey() -> String? {
        //Since it's the only one that is not optional and server doesn't provide ids for elements
        return "productDescription"
    }
}

extension ProductEntity {
    static var templateEntity: ProductEntity {
        let entity = ProductEntity()
        return entity
    }
}

